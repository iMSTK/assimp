What is this project ?
----------------------

This project is **NOT** the official assimp repository.

It is a fork of assimp sources hosted at https://github.com/assimp/assimp.

It is used as staging area to maintain topics specific to iMSTK that will eventually be contributed back to the official repository.


What is the branch naming convention ?
--------------------------------------

Each branch is named following the pattern `imstk-vX.Y.Z-YYYY-MM-DD-SHA{N}`

where:

* `vX.Y.Z` is the version of assimp
* `YYYY-MM-DD` is the date of the last official commit associated with the branch.
* `SHA{N}` are the first N characters of the last official commit associated with the branch.


What is the title convention for commits ?
------------------------------------------

There are few scenarios to consider:

| Commit status | Title convention |
|--|--|
| already integrated upstream | Amend the commit title to include `[Backport] This is the original title`
| being considider for upstream integration | Amend the commit title to include `[Backport PR-<NNN>] This is the original title` where `<NNN>` identifies the associated pull request being reviewed. |
| applies only to iMSTK | If the commit will not be considered for upstream integration, consider using the prefix `[iMSTK] This is the title` |


Are there exceptions to the branch naming convention ?
------------------------------------------------------

Yes, the only exception is the [fixCompilationError][] branch.

Following iMSTK commit [52b5f6135][], assimp archive was being downloaded only based on the name of the branch. This means that we need to keep this branch to ensure version of iMSTK referencing it can still be built.

As of 2022-02-10, the equivalent branch following the established naming convention is [imstk-v3.3.1-2016-07-08-a8673d482][].

[fixCompilationError]: https://gitlab.kitware.com/iMSTK/assimp/-/commits/fixCompilationError/
[imstk-v3.3.1-2016-07-08-a8673d482]: https://gitlab.kitware.com/iMSTK/assimp/-/commits/imstk-v3.3.1-2016-07-08-a8673d482/
[52b5f6135]: https://gitlab.kitware.com/iMSTK/iMSTK/-/commit/52b5f61354c71698fd5d8038d4889cf3bd591fe6

